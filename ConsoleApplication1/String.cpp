/*
* String.cpp
*
*  Created on: 2018-10-19
*      Author: mazs
*/

#include "String.h"
#include "string.h"
#include <assert.h>
#include <stdio.h>
#include <iostream>

namespace mzs {
	
	String::String() 
	{
		length = 0;
		this->s_str = NULL;
	}

	String::String(const char *str) 
	{
		if (str == NULL) {
			*s_str = '\0';
		}
		else {
			DEARR(s_str);
			length = strlen(str);
			s_str = new char[length + 1];
			strcpy_s(s_str, length + 1, str);
		}
	}

	String::String(const String &str) 
	{
		DEARR(s_str);
		length = strlen(str.s_str) + 1;
		s_str = new char[length];
		strcpy_s(s_str, length, str.s_str);
	}

	String::String(const std::string str)
	{
		DEARR(s_str);
		length = str.size() + 1;
		s_str = new char[str.size() + 1];
		strcpy_s(s_str, length, str.c_str());
	}

	String::String(const short t)
	{
		DEARR(s_str);
		s_str = new char[SHORT_LEN];
		_itoa_s(t, s_str, SHORT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const int i)
	{
		DEARR(s_str);
		s_str = new char[INT_LEN];
		_itoa_s(i, s_str, INT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const long long ld) 
	{
		DEARR(s_str);
		s_str = new char[LONG_LEN];
		_i64toa_s(ld, s_str, LONG_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const unsigned short t)
	{
		DEARR(s_str);
		s_str = new char[SHORT_LEN];
		_ui64toa_s(t, s_str, SHORT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const unsigned int i)
	{
		DEARR(s_str);
		s_str = new char[INT_LEN];
		_ui64toa_s(i, s_str, INT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const unsigned long long l)
	{
		DEARR(s_str);
		s_str = new char[LONG_LEN];
		_ui64toa_s(l, s_str, LONG_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
	}

	String::String(const float f)
	{
		DEARR(s_str);
		s_str = new char[FLOAT_LEN];
		_gcvt_s(s_str, FLOAT_LEN, f, 8);
		length = strlen(s_str);
	}

	String::String(const double d)
	{
		DEARR(s_str);
		s_str = new char[DOUBLE_LEN];
		_gcvt_s(s_str, DOUBLE_LEN, d, 16);
		length = strlen(s_str);
	}

	String::~String() {
		if (s_str != NULL) {
			DEARR(s_str);
		}
	}

	char* String::toChar() {
		return s_str;
	}

	std::string String::toString() {
		return toChar();
	}

	size_t String::size() {
		return length;
	}

	char &String::operator[](int i) {
		return s_str[i];
	}

	String &String::operator=(const String &s) {
		if (!strcmp(s_str , s.s_str)) {
			return *this;
		}

		if (this==&s){
			return *this;
		}

		if (s_str != NULL) {
			delete[] s_str;
			s_str = NULL;
		}

		length = strlen(s.s_str);
		s_str = new char[length + 1];
		strcpy_s(s_str, length + 1, s.s_str);

		return *this;
	}

	String &String::operator=(char *s) {
		
		if (&(s_str) == &s) {
			return *this;
		}

		DEARR(s_str);

		length = strlen(s);
		s_str = new char[length + 1];
		strcpy_s(s_str, length + 1, s);

		return *this;
	}

	String &String::operator=(std::string s) {
		return String::operator=((char*)s.c_str());
	}

	std::ostream &operator<<(std::ostream&os, String&s) {
		os << s.s_str;
		return os;
	}

	std::istream &operator >> (std::istream &is, String &s) {
		DEARR(s.s_str);
		char temp[2048] = { 0 };
		std::cin >> temp;

		s.length = strlen(temp);
		s.s_str = new char[s.length + 1];
		strcpy_s(s.s_str, s.length + 1, temp);

		return is;
	}

	String String::operator+(String &s) {

		s.length = strlen(s_str) + strlen(s.s_str) + 1;
		size_t index = strlen(s_str);
		s_str = (char*)_recalloc(s_str, s.length , sizeof(char));
		if (s_str)
			strcpy_s(s_str + index, s.length, s.s_str);

		return s_str;
	}

	String String::operator +(char* s) {

		length = strlen(s_str) + strlen(s) + 1;
		size_t index = strlen(s_str);
		s_str = (char*)_recalloc(s_str, length , sizeof(char));
		if (s_str)
			strcpy_s(s_str + index, length, s);

		return s_str;
	}

	String String::operator +(const std::string& s) {
		return String::operator +((char*)s.c_str());
	}

	String &String::operator+=(String &s) {

		append(s_str, length, s.toChar(), s.size());

		return *this;
	}

	String& String::operator +=(char* s) {
		String temp = s;
		return String::operator+=(temp);
	}

	String& String::operator +=(std::string& s) {
		return String::operator+=((char*)s.c_str());
	}

	String &String::operator+=(short t)
	{
		String temp = t;
		return String::operator+=(temp);
	}

	String &String::operator+=(int d)
	{
		String temp = d;
		return String::operator+=(temp);
	}

	String &String::operator+=(long long l)
	{
		String temp = l;
		return String::operator+=(temp);
	}

	String &String::operator+=(float f)
	{
		String temp = f;
		return String::operator+=(temp);
	}

	String &String::operator+=(double lf)
	{
		String temp = lf;
		return String::operator+=(temp);
	}

	String &String::operator=(short t) {

		DEARR(s_str);
		s_str = new char[SHORT_LEN];
		_itoa_s(t, s_str, SHORT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator=(int d) {

		DEARR(s_str);
		s_str = new char[INT_LEN];
		_itoa_s(d, s_str, INT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator = (long long ld)
	{
		DEARR(s_str);
		s_str = new char[LONG_LEN];
		_i64toa_s(ld, s_str, LONG_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator=(unsigned short t)
	{
		DEARR(s_str);
		s_str = new char[SHORT_LEN];
		_ui64toa_s(t, s_str, SHORT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator=(unsigned int d)
	{
		DEARR(s_str);
		s_str = new char[INT_LEN];
		_ui64toa_s(d, s_str, INT_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator=(unsigned long long ld)
	{
		DEARR(s_str);
		s_str = new char[LONG_LEN];
		_ui64toa_s(ld, s_str, LONG_LEN, 10);
		length = strlen(s_str);
		s_str[length] = '\0';
		return *this;
	}

	String &String::operator=(float f) {

		DEARR(s_str);
		s_str = new char[FLOAT_LEN];
		_gcvt_s(s_str, FLOAT_LEN, f, 8);//8为小数长度不包含'.'
		length = strlen(s_str);
		return *this;
	}

	String &String::operator=(double d) {

		DEARR(s_str);
		s_str = new char[DOUBLE_LEN];
		_gcvt_s(s_str, DOUBLE_LEN, d, 16);//16为小数长度不包含'.'
		length = strlen(s_str);
		return *this;
	}

	int String::toInt(char *s)
	{
		return atoi(s);
	}

	long String::toLong(char *s)
	{
		return atoi(s);
	}
	float String::toFloat(char *s)
	{
		return atof(s);
	}
	double String::toDouble(char *s)
	{
		return atof(s);
	}
	int String::toInt(const String s)
	{
		s_str = ((String)s).toChar();
		return atoi(s_str);
	}
	long String::toLong(const String s)
	{
		s_str = ((String)s).toChar();
		return atoi(s_str);
	}
	float String::toFloat(const String s)
	{
		s_str = ((String)s).toChar();
		return atof(s_str);
	}
	double String::toDouble(const String s)
	{
		s_str = ((String)s).toChar();
		return atof(s_str);
	}


	bool String::operator ==(const String& s) {
		if (strcmp(s_str, s.s_str) == 0) {
			return true;
		}
		return false;
	}

	bool String::operator !=(const String& s) {
		if (strcmp(s_str, s.s_str) == 0) {
			return false;
		}
		return true;
	}

	String  String::sub_to(int i, int n)
	{
		length = strlen(s_str);
		char* temp = new char[length + 1];
		memset(temp, 0, length+1 );

		temp[length] = '\0';
		memcpy(temp, s_str + n, i);
		delete[] s_str; s_str = NULL;

		length = strlen(temp) ;
		s_str = new char[length + 1];
		strcpy_s(s_str, length + 1, temp);
		delete[] temp; temp = NULL;

		return *this;
	}

	String String::sub_to(char * mark, char** cstr, int slipt)
	{
		if (s_str == ""&&mark == "")
			return NULL;
		int markIndex = 0;
		int continuous = 0;
		size_t markLength = strlen(mark);
		int i = 0;
		for (; s_str[i] != '\0'; i++)
		{
			if (s_str[i] == mark[markIndex])
			{
				markIndex = (markIndex++ >= markLength ? 0 : markIndex);
				continuous++;
			}
			else
			{
				markIndex = 0;
				continuous = 0;
			}

			if (continuous == markLength)
			{
				continuous = 0;
				size_t slipt_ = 0;
				int index = i - (int)markLength + 1;

				if (slipt >= 0)
					slipt_ = (slipt > length) ? length : slipt;
				else
					slipt_ = (slipt > -index) ? slipt : -index;

				size_t len = index + slipt_;
				size_t len_ = length - len;
				char*temp = new char[len+1];
				temp[len] = '\0';
				memcpy(temp, s_str, len);
				*cstr = new char[len_ + 1];
				(*cstr)[len_] = '\0';
				memcpy(*cstr, s_str + len, len_);
				length = len;

				if (s_str!=NULL)
				{
					delete[] s_str; s_str = NULL;
				}
				s_str = temp;
				return *this;
			}
		}
		return NULL;
	}

	//自动开辟出下一次的内存,不推荐
	int String::append(char* &beginBytes, size_t &beginLength, char *nextBytes, size_t nextLength)
	{
		if (beginBytes == NULL&&nextBytes == NULL)
			return -1;

		char *tempArray = NULL;
		tempArray = (char *)calloc((beginLength + nextLength + 1), sizeof(char));
		if (tempArray&&beginLength >0)
		{
			memcpy(tempArray, beginBytes, beginLength);
			free(beginBytes); beginBytes = NULL;
		}

		if (tempArray!=nullptr)
		{
			memcpy(tempArray + beginLength, nextBytes, nextLength);
			beginLength += nextLength;
			tempArray[beginLength] = '\0';
			beginBytes = tempArray;
		}

		return 0;
	}

} ////////////////mzs//end///////////////////////


//int getLength(long i)
//{
//	int len = 1;
//	if (i<0) i = -i;
//	for (; i / 10>0; i /= 10)
//		++len;
//	return len;
//}
