/*
 * String.h
 *
 *  Created on: 2018-10-19
 *      Author: mazs
 */

#ifndef SRC_UTIL_STRING_H_
#define SRC_UTIL_STRING_H_

#include <list>
#include <stdlib.h>
#include <iostream>
#include <string>

#define dll __declspec(dllexport)


# define SHORT_LEN  16
# define INT_LEN  32
# define LONG_LEN   64
# define FLOAT_LEN   128
# define DOUBLE_LEN   128
#define DEARR(v) v != NULL? delete[](v), v = NULL: NULL


namespace mzs {

	class dll String {
	public:

		String();
		String(const char *str);
		String(const String &str);
		String(const std::string str);
		String(const short t);
		String(const int i);
		String(const long long l);
		String(const unsigned short t);
		String(const unsigned int i);
		String(const unsigned long long l);
		String(const float f);
		String(const double d);
		virtual ~String();

		//重载操作符[]
		char &operator[](int i);
		//重载操作符<<
		friend std::ostream &operator<<(std::ostream &os, String &s);
		//重载操作符>>
		friend std::istream &operator>>(std::istream &is, String &s);
		//重载操作符==
		bool operator==(const String &s);
		//重载操作符!=
		bool operator!=(const String &s);

		//重载操作符=
		String &operator=(const String &s);
		String &operator=(std::string s);
		String &operator=(char *s);

		String &operator=(short t);
		String &operator=(int d);
		String &operator=(long long l);
		String &operator=(unsigned short t);
		String &operator=(unsigned int d);
		String &operator=(unsigned long long l);
		String &operator=(float f);
		String &operator=(double lf);

		char*  toChar();

		int    toInt(char *s);
		long   toLong(char *s);
		float  toFloat(char *s);
		double toDouble(char *s);

		int    toInt(const String s);
		long   toLong(const String s);
		float  toFloat(const String s);
		double toDouble(const String s);

		//重载操作符+
		String operator+(String &s);
		String operator+(const std::string &s);
		String operator+(char *s);
		//重载操作符+=
		String &operator+=(String &s);
		String &operator+=(std::string &s);
		String &operator+=(char *s);
		String &operator+=(short t);
		String &operator+=(int d);
		String &operator+=(long long l);
		String &operator+=(float f);
		String &operator+=(double lf);

		//类型转换
		std::string toString();
		//获取自身长度
		size_t size();

		/*
		* @描述：截取i到n部分。
		*/
		String sub_to(int i, int n = 0);

		/*
		* @描述：字符串截取分前后两部分。
		* 参数str  ：原始字符串；
		* 参数mark ：截取字符串定位符号（一个字符，例如‘，’）；
		* 参数cstr：返回从0到mark截取之间字符串；
		* -参数slipt：向后移动指针索引位置(非必选)；
		* return 从mark截取后剩下的字符串。
		*/
		String sub_to(char * mark, char** cstr = NULL, int slipt = 0);

	private:
		//自动开辟出下一次的内存
		int append(char* &beginBytes, size_t &beginLength, char *nextBytes, size_t nextLength);

		char* s_str = NULL;
		size_t length = 0;
	};

} 
#endif /* namespace mzs */
